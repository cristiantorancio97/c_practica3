#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void imprimir_texto_ayuda(void);
void imprimir_argumentos(char const *argv[],int argc);

int main(int argc, char const *argv[])
{	
   	
   	static char *name[] = { "-s","-r","-m","-d","-i","-h" };

   	int i,j,k,a,b;

   	if(argc>1){
   		for(i=1;i<argc;i++){
   			if(!strcmp(*(argv+i),"-h")){
   				imprimir_texto_ayuda();
   				exit(1);
   			}
   		}
   		for(i=1;i<argc;i++){
	   		if(!strcmp(*(argv+i),"-s")){
	   			if(*(argv+i+1)&&*(argv+i+2)){
	   				a = atoi(*(argv+i+1));
	   				b = atoi(*(argv+i+2));
	   				printf("El resultado de la suma es %d\n",a+b);
	   			}else{
	   				exit(1);
	   			}
	   		}
	   		if(!strcmp(*(argv+i),"-r")){
	   			if(*(argv+i+1)&&*(argv+i+2)){
		   			a = atoi(*(argv+i+1));
		   			b = atoi(*(argv+i+2));
		   			printf("El resultado de la resta es %d\n",a-b);	
	   			}else{
	   				exit(1);
	   			}
	   		}
	   		if(!strcmp(*(argv+i),"-i")){
	   			printf("Los parametros ingresados son: ");
	   			for(j=1;j<argc;j++){
	   				printf("%s ",*(argv+j));
	   			}
	   			putchar('\n');
	   		}
   		}
   	}else{
   		printf("ingresar: \n");
   		for(k=0;k<6;k++){
   			printf("%s ",*(name+k));
   			if(!strcmp(*(name+k),"-s")||!strcmp(*(name+k),"-r")){
   				printf(" [numero numero] ");
   			}
   		}
   		putchar('\n');
   	}

	return 0;
}

void imprimir_argumentos(char const *argv[],int argc){
	int i;
	for(i=1;i<argc;i++){
		printf("%s ",*(argv+i));
	}
	putchar('\n');
}

void imprimir_texto_ayuda(void){
	printf("El programa imprime el resultado de enviar una operacion y sus correspondientes parametros. El resultado sera impreso en pantalla.\n");
	printf("Argumentos:\n");
	printf("-s: suma los siguientes 2 parametros.\n");
	printf("-r: resta los siguientes 2 parametros.\n");
	printf("-d: divide los siguientes 2 parametros.\n");
	printf("-m: multiplica los siguientes 2 parametros.\n");
	printf("-i: imprime todos los parametros recibidos.\n");
	printf("-h: imprime un texto de ayuda.\n");
}
