#include <stdio.h>

void reverso_iterativo(char *s,int t);
void reverso_recursivo(char *s,int t);

int main(int argc, char const *argv[])
{
	char s[] = "hola";
	int tam = sizeof(s)/sizeof(s[0]);
	reverso_iterativo(s,tam);
	reverso_recursivo(s,tam);
	return 0;
}

void reverso_recursivo(char *s,int t){
	if(t>0){
		t--;
		printf("%c",s[t]);
		reverso_recursivo(s,t);
	}else{
		putchar('\n');
	}
}

void reverso_iterativo(char *s,int t){
	int i;
	for(i=(t-1);i>=0;i--){
		putchar(*(s+i));
	}
	putchar('\n');
}