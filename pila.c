#define TAM 3
#include <stdio.h>
#include <stdlib.h>

char *crear_pila(int t);
void apilar(char *pchar,char c,int *tam);
char *desapilar(char *pchar,int *tam);
void destruir_pila(char *pchar,int *tam);

int main(int argc, char const *argv[])
{
	int tam = 0;
	char *pchar = NULL;

	pchar = crear_pila(TAM);

	

	int letra;

	while((letra=getchar())!=EOF&&(letra!='\n')){
		apilar(pchar,letra,&tam);
	}

	int i;
	for (i = 0; i<tam; ++i)
	{
		printf("%c",*(pchar+i));
	}
	putchar('\n');

	int t=tam;
	for(i=0;i<=t;i++){
		char *caracter = desapilar(pchar,&tam);
		printf("%c",*caracter);
	}
	putchar('\n');

	destruir_pila(pchar,&tam);

	printf("%d\n",tam);

	return 0;
}

char *crear_pila(int t){
	char *c;
	if(!(c=malloc(t*sizeof(char)))){
		printf("memoria insuficiente.\n");
		exit(1);
	}
	return c;
}

void apilar(char *pchar,char c,int *tam){
	if(*tam<TAM){
		*(pchar+*tam)=c;
		*tam+=1;
	}else{
		if(!(pchar = realloc(pchar,*tam*2*sizeof(char)))){
			printf("memoria insuficiente.\n");
			exit(1);
		}
		*(pchar+*tam)=c;
		*tam+=1;
	}
}

char *desapilar(char *pchar,int *tam){
	char *p = pchar;
	if(tam>0){
		*tam-=1;
		return p+*tam;
	}
}

void destruir_pila(char *pchar,int *tam){
	free(pchar);
	if(pchar==NULL){
		printf("puntero nulo.\n");
		exit(1);
	}else{
		pchar=NULL;
		*tam=0;
		printf("memoria liberada.\n");
	}
}