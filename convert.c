#include <stdio.h>

int enteroAcaracter(int n);
void convertir(int i, char s[], int b);

int main(int argc, char const *argv[])
{
	int i,b;
	char *s;
	i=15;
	b=2;
	convertir(i,s,b);
	printf("%s\n",s);
	return 0;
}

int enteroAcaracter(int n){
	(n>9) ? n+=55 : n+=48;
	return n;
}

void convertir(int i, char s[], int b){
	int j = 0;
	while(i){
		s[j]=(char)(enteroAcaracter(i%b));
		j++;
		i/=b;
	}
	s[j]='\0';
}