#define TAM 5
#include <stdio.h>
#include <stdlib.h>

int *redimensionar_vector(int *p,int t);

int main(int argc, char const *argv[])
{
	int *p = malloc(TAM*sizeof(int));
	int i;
	for(i=0;i<TAM;++i){
		*(p+i)=i;
	}
	p=redimensionar_vector(p,10);
	for(i=5;i<10;++i){
		*(p+i)=i;
	}
	for (i = 0; i < 10; ++i)
	{
		printf("%d ",*(p+i));
	}
	putchar('\n');
	return 0;
}

int *redimensionar_vector(int *p,int t){
	int *pint;
	if(!(pint=realloc(p,t*sizeof(int)))){
		printf("no existe memoria.\n");
		exit(1);
	}
	return pint;
}