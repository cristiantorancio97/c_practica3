#include <stdio.h>
#include <string.h>

int enteroAcaracter(int n);
void convertir(int i, char s[], int b);
void imprimir_arreglo_invertido(char *s,int t);

int main(int argc, char const *argv[])
{
	const char *cadena = "1234";
	int a,b;
	sscanf(argv[1],"%d",&a);
	sscanf(argv[2],"%d",&b);
	char *numero;
	convertir(a,numero,b);
	int size = sizeof(numero)/sizeof(numero[0]);
	imprimir_arreglo_invertido(numero,size);
	return 0;
}

int enteroAcaracter(int n){
	if(n>9) n+=55;
	else n+=48;
	return n;
}

void convertir(int i, char s[], int b){
	int j = 0;
	while(i){
		s[j]=(char)(enteroAcaracter(i%b));
		j++;
		i/=b;
	}
	s[j]='\0';
}

void imprimir_arreglo_invertido(char *s,int t){
	int i;
	for(i=(t-1);i>=0;i--){
		putchar(*(s+i));
	}
	putchar('\n');
}