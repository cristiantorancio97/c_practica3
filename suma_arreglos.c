#include <stdio.h>

void sumar_arreglos(int *a,int *b,int *c,int tam_a,int tam_b,int tam_c);

void imprimir_arreglo(int *a,int tam_a);

int main(int argc, char const *argv[])
{
	
	int tam_a,tam_b,tam_c;

	int a[4] = { 3, 2, 4, 4 };
	int b[2] = { 1, 4 };

	tam_a = sizeof(a)/sizeof(a[0]);
	tam_b = sizeof(b)/sizeof(b[0]);

	if(tam_a>=tam_b) tam_c=tam_a;
	else tam_c = tam_b;

	int c[tam_c];

	sumar_arreglos(a,b,c,tam_a,tam_b,tam_c);

	imprimir_arreglo(c,tam_c);

	return 0;
}

void imprimir_arreglo(int *a,int tam_a){
	int i;
	for(i=0;i<tam_a;++i){
		printf("%d ",*(a+i));
	}
	putchar('\n');
}

void sumar_arreglos(int *a,int *b,int *c,int tam_a,int tam_b,int tam_c){
	int t,i;
	if(tam_a<=tam_b) t=tam_a;
	else t=tam_b;

	for(i=0;i<t;++i){
		c[i]=a[i]+b[i];
	}

	if(tam_a<=tam_b){
		for(i=t;i<tam_c;++i){
			c[i]=b[i];
		}
	}else{
		for(i=t;i<tam_c;++i){
			c[i]=a[i];
		}
	}
	
}