#include <stdio.h>

void imprimirArreglo(int *a,int t);

void ordenar(int *a,int t);

int main(int argc, char const *argv[])
{
	int a[4] = {10,3,4,1};	

	int tam = sizeof(a)/sizeof(a[0]);

	imprimirArreglo(a,tam);

	ordenar(a,tam);

	imprimirArreglo(a,tam);

	return 0;
}

void ordenar(int *a, int tam){
	int i,j,temp,min,pos;
	for(i=0;i<(tam-1);++i){
		min = 999;
		for(j=i;j<tam;j++){
			if(a[j]<min){
				min=a[j];
				pos=j;
			}
		}
		temp = a[i];
		a[i]=min;
		a[pos]=temp;
	}
}

void imprimirArreglo(int *a,int t){
	int i;
	for(i=0;i<t;i++){
		printf("%d ",*(a+i));
	}
	putchar('\n');
}