#define TAM 10
#include <stdio.h>
#include <stdlib.h>

int *crear_vector_int(int n);
void imprimir_vector_int(int *a);
void asignar_vector_int(int *a);

int main(int argc, char const *argv[])
{
	int *p = crear_vector_int(TAM);
	asignar_vector_int(p);
	imprimir_vector_int(p);
	return 0;
}

int *crear_vector_int(int n){
	int *p;
	if(!(p=malloc(n*sizeof(int)))){
		printf("no existe memoria.\n");
		exit(1);
	}
	return p;
}

void asignar_vector_int(int *a){
	int i;
	for (i = 0; i < TAM; ++i)
	{
		*(a+i)=i;
	}
}

void imprimir_vector_int(int *a){
	int i;
	for (i = 0; i < TAM; ++i)
	{
		printf("%d ",*(a+i));
	}
	putchar('\n');
}